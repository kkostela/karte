from django.apps import AppConfig


class KarteappConfig(AppConfig):
    name = 'karteapp'
