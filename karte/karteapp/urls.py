from django.urls import path

from . import views

urlpatterns = [
	path('', views.karte, name="karte"),
	path('kosarica/', views.kosarica, name="kosarica"),
	path('dostava/', views.dostava, name="dostava"),
]