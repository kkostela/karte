from django.shortcuts import render

# Create your views here.

def karte(request):
    context = {}
    return render(request, 'karte/karte.html', context)

def kosarica(request):
    context = {}
    return render(request, 'karte/kosarica.html', context)

def dostava(request):
    context = {}
    return render(request, 'karte/dostava.html', context)